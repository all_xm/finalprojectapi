package shop.usehwa.gen.api.member.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.member.entity.Member;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Optional<Member> findByUsername(String username);
    long countByUsername(String username);
}