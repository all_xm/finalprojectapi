package shop.usehwa.gen.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.member.entity.Member;
import shop.usehwa.gen.api.member.entity.Team;
import shop.usehwa.gen.api.member.model.MemberRequest;
import shop.usehwa.gen.api.member.model.PasswordChangeRequest;
import shop.usehwa.gen.api.member.repository.MemberRepository;
import shop.usehwa.gen.api.member.repository.TeamRepository;
import shop.usehwa.gen.common.enums.MemberGroup;
import shop.usehwa.gen.common.exception.*;
import shop.usehwa.gen.common.function.CommonCheck;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;
    private final TeamRepository teamRepository;
    private final PasswordEncoder passwordEncoder;


    public void setFirstMember() {
        String username = "superadmin";
        String password = "abcd1234";
        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            MemberRequest createRequest = new MemberRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setPasswordRe(password);
            createRequest.setMemberGroup(MemberGroup.ROLE_ADMIN);

            setMember(createRequest);
        }
    }

    public void setMember(MemberRequest request) {
        if (!CommonCheck.checkUsername(request.getUsername()))
            throw new CWrongPhoneNumberException(); // 유효한 아이디 형식이 아닙니다로 던지기
        if (!request.getPassword().equals(request.getPasswordRe()))
            throw new CWrongPhoneNumberException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(request.getUsername())) throw new CUsernameSignInFailedException(); // 중복된 아이디가 존재합니다 던지기

        request.setPassword(passwordEncoder.encode(request.getPassword()));

        Member member = new Member.MemberBuilder(request).build();
        memberRepository.save(member);
    }

    public void setMember(MemberRequest request, long teamId) {
        if (!CommonCheck.checkUsername(request.getUsername()))
            throw new CWrongUserNameTypeException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!request.getPassword().equals(request.getPasswordRe()))
            throw new CWrongPasswordException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(request.getUsername())) throw new CDuplicationUsernameException(); // 중복된 아이디가 존재합니다 던지기

        Team team = teamRepository.findById(teamId).orElseThrow(CMissingDataException::new);

        request.setPassword(passwordEncoder.encode(request.getPassword()));

        Member member = new Member.MemberBuilder(request).setTeam(team).build();
        memberRepository.save(member);
    }

    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;
    }

    public void putPassword(long memberId, PasswordChangeRequest changeRequest, boolean isAdmin) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);

        if (!isAdmin && !passwordEncoder.matches(changeRequest.getChangePassword(), member.getPassword()))
            throw new CWrongPasswordException(); // 비밀번호가 일치하지 않습니다 던지기

        String passwordResult = passwordEncoder.encode(changeRequest.getChangePassword());
        member.putPassword(passwordResult);
        memberRepository.save(member);
    }
}