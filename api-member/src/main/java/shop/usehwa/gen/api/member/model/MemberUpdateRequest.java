package shop.usehwa.gen.api.member.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberUpdateRequest {
    @NotNull
    private Boolean isUse;
}
