package shop.usehwa.gen.api.member.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.usehwa.gen.api.member.model.TeamRequest;
import shop.usehwa.gen.api.member.model.TeamUpdateRequest;
import shop.usehwa.gen.api.member.service.TeamService;
import shop.usehwa.gen.common.response.model.CommonResult;
import shop.usehwa.gen.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "팀 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/team")
public class TeamController {
    private final TeamService teamService;

    @ApiOperation(value = "팀 등록")
    @PostMapping("/add")
    public CommonResult setTeam(@RequestBody @Valid TeamRequest request) {
        teamService.setTeam(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "팀 조회")
    @GetMapping("/list")
    public CommonResult getTeams() {
        return ResponseService.getListResult(teamService.getTeams(), true);
    }

    @ApiOperation(value = "팀 수정")
    @PutMapping("/correction/{id}")
    public CommonResult putTeam(@PathVariable long id, @RequestBody @Valid TeamUpdateRequest request) {
        teamService.putTeam(id, request);
        return ResponseService.getSuccessResult();
    }
}