package shop.usehwa.gen.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.member.entity.Team;
import shop.usehwa.gen.api.member.model.TeamItem;
import shop.usehwa.gen.api.member.model.TeamRequest;
import shop.usehwa.gen.api.member.model.TeamUpdateRequest;
import shop.usehwa.gen.api.member.repository.TeamRepository;
import shop.usehwa.gen.common.exception.CDontFindTeamInfoException;
import shop.usehwa.gen.common.response.model.ListResult;
import shop.usehwa.gen.common.response.service.ListConvertService;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TeamService {
    private final TeamRepository teamRepository;

    public void setTeam(TeamRequest request) {
        Team addData = new Team.TeamBuilder(request).build();

        teamRepository.save(addData);
    }

    public ListResult<TeamItem> getTeams() {
        List<Team> originList = teamRepository.findAll();

        if (originList.isEmpty()) throw new CDontFindTeamInfoException();

        List<TeamItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new TeamItem.TeamItemBuilder(item).build()));
        return ListConvertService.settingResult(result);
    }

    public void putTeam(long id, TeamUpdateRequest request) {
        Team originData = teamRepository.findById(id).orElseThrow(CDontFindTeamInfoException::new);
        originData.putTeam(request);

        teamRepository.save(originData);
    }
}