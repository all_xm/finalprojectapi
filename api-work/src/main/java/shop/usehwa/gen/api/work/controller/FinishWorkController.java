package shop.usehwa.gen.api.work.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.usehwa.gen.api.work.model.WorkFinishRequest;
import shop.usehwa.gen.api.work.service.FinishWorkService;
import shop.usehwa.gen.common.response.model.CommonResult;
import shop.usehwa.gen.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "작업 완료")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/finish/work")
public class FinishWorkController {
    private final FinishWorkService finishWorkService;

    @ApiOperation(value = "업무 인계")
    @PutMapping("/update/{workStatusId}/{workRecordId}")
    public CommonResult finishWork(@RequestBody @Valid WorkFinishRequest request,
                                   @PathVariable long workStatusId, @PathVariable long workRecordId) {
        finishWorkService.putFinishWork(request, workStatusId, workRecordId);

        return ResponseService.getSuccessResult();
    }
}
