package shop.usehwa.gen.api.work.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.work.entity.WorkRecord;

import java.time.LocalDateTime;
import java.util.List;

public interface WorkRecordRepository extends JpaRepository<WorkRecord, Long> {
    List<WorkRecord> findByTeam_Id(long teamId);
    List<WorkRecord> findByContract_Id(long teamId);
    List<WorkRecord> findByCreateDateBetween (LocalDateTime startDate, LocalDateTime endDate);
}
