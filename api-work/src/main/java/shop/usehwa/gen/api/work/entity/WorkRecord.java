package shop.usehwa.gen.api.work.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.work.model.WorkRecordCreateRequest;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WorkRecord {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "공정 관리 시퀀스")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "processId")
    private Process process;

    @ApiModelProperty(notes = "팀 시퀀스")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "teamId")
    private Team team;

    @ApiModelProperty(notes = "사진 이미지 주소")
    @Column(length = 200)
    private String imageAddress;

    @ApiModelProperty(notes = "메모")
    @Column(columnDefinition = "TEXT")
    private String memo;

    @ApiModelProperty(notes = "작업자명")
    @Column(nullable = false, length = 20)
    private String workerName;

    @ApiModelProperty(notes = "계약서 관리 시퀀스")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "contractId")
    private Contract contract;

    @ApiModelProperty(notes = "등록일시")
    @Column(nullable = false)
    private LocalDateTime createDate;

    private WorkRecord(WorkRecordBuilder builder) {
        this.process = builder.process;
        this.team = builder.team;
        this.imageAddress = builder.imageAddress;
        this.memo = builder.memo;
        this.workerName = builder.workerName;
        this.contract = builder.contract;
        this.createDate = builder.createDate;
    }

    public static class WorkRecordBuilder implements CommonModelBuilder<WorkRecord> {

        private final Process process;
        private final Team team;
        private final String imageAddress;
        private final String memo;
        private final String workerName;
        private final Contract contract;
        private final LocalDateTime createDate;

        public WorkRecordBuilder(WorkRecordCreateRequest request, Team team, Contract contract) {
            this.process = contract.getPresentProcess();
            this.team = team;
            this.imageAddress = request.getImageAddress();
            this.memo = request.getMemo();
            this.workerName = request.getWorkerName();
            this.contract = contract;
            this.createDate = LocalDateTime.now();
        }

        @Override
        public WorkRecord build() {
            return new WorkRecord(this);
        }
    }
}
