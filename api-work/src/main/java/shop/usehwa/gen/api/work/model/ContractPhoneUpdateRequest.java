package shop.usehwa.gen.api.work.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ContractPhoneUpdateRequest {
    @ApiModelProperty(notes = "전화번호")
    @NotNull
    @Length(min = 13, max = 13)
    private String phoneNumber;
}
