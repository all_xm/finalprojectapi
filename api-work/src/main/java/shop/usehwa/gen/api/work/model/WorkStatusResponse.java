package shop.usehwa.gen.api.work.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.work.entity.WorkStatus;
import shop.usehwa.gen.common.function.ConvertFormat;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WorkStatusResponse {
    private String approver;

    private String createDate;

    private String isCompleteText;

    private String contractNumber;

    private String processName;

    private String teamName;

    private String mainWorkRecordImgName;

    private String mainWorkRecordMemo;

    private WorkStatusResponse(WorkStatusResponseBuilder builder) {
        this.approver = builder.approver;
        this.createDate = builder.createDate;
        this.isCompleteText = builder.isCompleteText;
        this.contractNumber = builder.contractNumber;
        this.processName = builder.processName;
        this.teamName = builder.teamName;
        this.mainWorkRecordImgName = builder.mainWorkRecordImgName;
        this.mainWorkRecordMemo = builder.mainWorkRecordMemo;
    }

    public static class WorkStatusResponseBuilder implements CommonModelBuilder<WorkStatusResponse> {

        private final String approver;
        private final String createDate;
        private final String isCompleteText;
        private final String contractNumber;
        private final String processName;
        private final String teamName;
        private final String mainWorkRecordImgName;
        private final String mainWorkRecordMemo;

        public WorkStatusResponseBuilder(WorkStatus workStatus) {
            this.approver = workStatus.getApprover();
            this.createDate = ConvertFormat.convertLocalDateTimeToStringMini(workStatus.getCreateDate());
            this.isCompleteText = workStatus.getIsComplete() ? "완료" : "진행 중";
            this.contractNumber = workStatus.getContract().getContractNumber();
            this.processName = workStatus.getProcess().getProcessName();
            this.teamName = workStatus.getTeam().getTeamName();
            this.mainWorkRecordImgName = workStatus.getWorkRecord().getImageAddress();
            this.mainWorkRecordMemo = workStatus.getWorkRecord().getMemo();
        }

        @Override
        public WorkStatusResponse build() {
            return new WorkStatusResponse(this);
        }
    }
}
