package shop.usehwa.gen.api.work.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import shop.usehwa.gen.common.enums.MemberGroup;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Member {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "아이디")
    @Column(nullable = false, length = 20, unique = true)
    private String username;

    @ApiModelProperty(notes = "비밀번호")
    @Column(nullable = false)
    private String password;

    @ApiModelProperty(notes = "권한")
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private MemberGroup memberGroup;

    @ApiModelProperty(notes = "팀 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "teamId")
    private Team team;

    @ApiModelProperty(notes = "사용 중 여부")
    @Column(nullable = false)
    private Boolean isUse;

}
