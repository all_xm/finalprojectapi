package shop.usehwa.gen.api.work.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.push.service.AndroidPushService;
import shop.usehwa.gen.api.work.entity.*;
import shop.usehwa.gen.api.work.entity.Process;
import shop.usehwa.gen.api.work.model.WorkFinishRequest;
import shop.usehwa.gen.api.work.repository.*;
import shop.usehwa.gen.common.exception.CNotRegistrationWorkRecordException;
import shop.usehwa.gen.common.exception.CNotRegistrationWorkStatusException;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FinishWorkService {
    private final ContractRepository contractRepository;
    private final ProcessRepository processRepository;
    private final WorkStatusRepository workStatusRepository;
    private final WorkRecordRepository workRecordRepository;
    private final AppTokenRepository appTokenRepository;

    public void putFinishWork(WorkFinishRequest request, long workStatusId, long workRecordId) {
        WorkStatus workStatus = workStatusRepository.findById(workStatusId).orElseThrow(CNotRegistrationWorkStatusException::new);
        WorkRecord workRecord = workRecordRepository.findById(workRecordId).orElseThrow(CNotRegistrationWorkRecordException::new);
        workStatus.finishWork(request, workRecord);

        workStatusRepository.save(workStatus);

        Contract contract = workStatus.getContract();
        PackageInfo packageInfo = contract.getPackageInfo();

        List<Process> processList = processRepository.findByPackageInfoOrderByProcessOrderAsc(packageInfo);

        Process process = processList.get(contract.getPresentProcess().getProcessOrder());

        contract.finishWork(process);

        contractRepository.save(contract);

        WorkStatus addData = new WorkStatus.WorkStatusBuilder(contract).build();

        workStatusRepository.save(addData);

        AppToken appToken = appTokenRepository.findByContract(workRecord.getContract());
        List<String> tokenResult = new LinkedList<>();
        tokenResult.add(appToken.getAppToken());
        new AndroidPushService().sendPush(tokenResult, "알림 확인", "진행 상황이 갱신되었습니다. 자세한 내용은 앱에서 확인해주세요.");
    }
}
