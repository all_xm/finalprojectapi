package shop.usehwa.gen.api.work.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.work.entity.AppToken;
import shop.usehwa.gen.api.work.entity.Contract;

import java.util.Optional;

public interface AppTokenRepository extends JpaRepository<AppToken, Long> {
    AppToken findByContract(Contract contract);
}
