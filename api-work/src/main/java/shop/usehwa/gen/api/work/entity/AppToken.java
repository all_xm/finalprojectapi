package shop.usehwa.gen.api.work.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class AppToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "contractId", nullable = false)
    private Contract contract;

    @Column(nullable = false)
    private String appToken;

    @Column(nullable = false)
    private LocalDateTime dateCreate;
}
