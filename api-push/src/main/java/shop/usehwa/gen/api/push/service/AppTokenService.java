package shop.usehwa.gen.api.push.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.push.entity.AppToken;
import shop.usehwa.gen.api.push.entity.Contract;
import shop.usehwa.gen.api.push.repository.AppTokenRepository;
import shop.usehwa.gen.common.exception.CMissingDataException;
import shop.usehwa.gen.common.function.CommonDate;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AppTokenService {
    private final AppTokenRepository appTokenRepository;

    public List<String> getSingleToken(Contract contract) { // 해당 계약의 앱 토큰을 가져오기
        List<String> result = new LinkedList<>();

        AppToken token = appTokenRepository.findByContract(contract).orElseThrow(CMissingDataException::new);
        result.add(token.getAppToken());

        return result;
    }

    public void settingToken(Contract contract, String token) {
        long checkCount = appTokenRepository.countByContract(contract);
        if (checkCount > 0) {
            putAppToken(contract, token);
        } else {
            setAppToken(contract, token);
        }
    }

    private void setAppToken(Contract contract, String token) {
        AppToken appToken = new AppToken();
        appToken.setContract(contract);
        appToken.setAppToken(token);
        appToken.setDateCreate(CommonDate.getNowTime());
        appTokenRepository.save(appToken);
    }

    private void putAppToken(Contract contract, String token) {
        AppToken appToken = appTokenRepository.findByContract(contract).orElseThrow(CMissingDataException::new);
        appToken.setAppToken(token);
        appToken.setDateCreate(CommonDate.getNowTime());
        appTokenRepository.save(appToken);
    }
}
