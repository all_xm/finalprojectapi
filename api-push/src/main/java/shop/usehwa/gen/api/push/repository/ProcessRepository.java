package shop.usehwa.gen.api.push.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.push.entity.PackageInfo;
import shop.usehwa.gen.api.push.entity.Process;

import java.util.List;

public interface ProcessRepository extends JpaRepository<Process, Long> {
    List<Process> findAllByIsUseAndPackageInfo(Boolean isUse, PackageInfo packageInfo);
    List<Process> findByPackageInfoOrderByProcessOrderAsc(PackageInfo packageInfo);
}
