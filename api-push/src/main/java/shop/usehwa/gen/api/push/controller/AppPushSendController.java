package shop.usehwa.gen.api.push.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.usehwa.gen.api.push.entity.Contract;
import shop.usehwa.gen.api.push.model.SendPushRequest;
import shop.usehwa.gen.api.push.service.AndroidPushService;
import shop.usehwa.gen.api.push.service.AppTokenService;
import shop.usehwa.gen.api.push.service.ContractDataService;
import shop.usehwa.gen.common.response.model.CommonResult;
import shop.usehwa.gen.common.response.service.ResponseService;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "앱 푸시 발송 관리")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/app-push")
public class AppPushSendController {
    private final AndroidPushService androidPushService;
    private final AppTokenService appTokenService;
    private final ContractDataService contractDataService;

    @ApiOperation(value = "개별 푸시")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "회원시퀀스", required = true),
    })
    @PostMapping("/send/person/{contractId}")
    public CommonResult sendPerson(@PathVariable long contractId, @RequestBody @Valid SendPushRequest sendPushRequest) {
        Contract contract = contractDataService.getData(contractId);

        List<String> sendTokens = appTokenService.getSingleToken(contract);
        androidPushService.sendPush(sendTokens, "[개인알림] " + sendPushRequest.getPushTitle(), sendPushRequest.getPushBody());

        return ResponseService.getSuccessResult();
    }
}
