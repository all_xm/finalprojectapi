package shop.usehwa.gen.api.push.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.push.entity.WorkStatus;

import java.util.List;

public interface WorkStatusRepository extends JpaRepository<WorkStatus, Long> {
    List<WorkStatus> findAllByContract_ContractNumberAndContract_CustomerPhone(String contractNumber, String customerPhone);
}
