package shop.usehwa.gen.api.push.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.push.entity.Contract;
import shop.usehwa.gen.api.push.repository.ContractRepository;
import shop.usehwa.gen.common.exception.CMissingDataException;

@Service
@RequiredArgsConstructor
public class ContractDataService {
    private final ContractRepository contractRepository;

    public Contract getData(long id) {
        return contractRepository.findById(id).orElseThrow(CMissingDataException::new);
    }
}
