package shop.usehwa.gen.api.push.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.push.entity.AppToken;
import shop.usehwa.gen.api.push.entity.Contract;


import java.util.List;
import java.util.Optional;

public interface AppTokenRepository extends JpaRepository<AppToken, Long> {
    Optional<AppToken> findByContract(Contract contract);
    long countByContract(Contract contract);
}
