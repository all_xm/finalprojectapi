package shop.usehwa.gen.api.push.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class PackageInfo {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "패키지명")
    @Column(nullable = false, length = 20)
    private String packageName;

    @ApiModelProperty(notes = "사용 중 여부")
    @Column(nullable = false)
    private Boolean isUse;

    @ApiModelProperty(notes = "등록일자")
    @Column(nullable = false)
    private LocalDateTime createDate;

    @ApiModelProperty(notes = "메모")
    @Column(nullable = false, columnDefinition = "TEXT")
    private String memo;

}
