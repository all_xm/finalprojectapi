package shop.usehwa.gen.api.push.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerLoginRequest {
    @ApiModelProperty(required = true, notes = "계약 번호(5~20자)")
    @NotNull
    @Length(min = 5, max = 20)
    private String contractNumber;

    @ApiModelProperty(required = true, notes = "고객 전화번호 (13자)")
    @NotNull
    @Length(min = 13, max = 13)
    private String customerPhone;

    @ApiModelProperty(required = true, notes = "앱토큰")
    @NotNull
    private String token;
}
