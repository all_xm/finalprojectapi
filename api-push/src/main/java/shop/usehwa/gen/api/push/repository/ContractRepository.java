package shop.usehwa.gen.api.push.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.usehwa.gen.api.push.entity.Contract;

import java.util.Optional;

public interface ContractRepository extends JpaRepository<Contract, Long> {
    Contract findByContractNumber(String contractNumber);
}
