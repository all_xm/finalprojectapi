package shop.usehwa.gen.api.push.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import shop.usehwa.gen.common.enums.ResultCode;
import shop.usehwa.gen.common.exception.*;
import shop.usehwa.gen.common.response.model.CommonResult;
import shop.usehwa.gen.common.response.service.ResponseService;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CAccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAccessDeniedException e) {
        return ResponseService.getFailResult(ResultCode.ACCESS_DENIED);
    }

    @ExceptionHandler(CAuthenticationEntryPointException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAuthenticationEntryPointException e) {
        return ResponseService.getFailResult(ResultCode.AUTHENTICATION_ENTRY_POINT);
    }

    @ExceptionHandler(CUsernameSignInFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CUsernameSignInFailedException e) {
        return ResponseService.getFailResult(ResultCode.USERNAME_SIGN_IN_FAILED);
    }

    @ExceptionHandler(CWrongPasswordException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CWrongPasswordException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PASSWORD);
    }

    @ExceptionHandler(CWrongUserNameTypeException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CWrongUserNameTypeException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_USERNAME_TYPE);
    }

    @ExceptionHandler(CDuplicationUsernameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDuplicationUsernameException e) {
        return ResponseService.getFailResult(ResultCode.DUPLICATION_USERNAME);
    }


    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CWrongPhoneNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPhoneNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PHONE_NUMBER);
    }

    @ExceptionHandler(CDontFindContractException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDontFindContractException e) {
        return ResponseService.getFailResult(ResultCode.DONT_FIND_CONTRACT);
    }

    @ExceptionHandler(CDontFindFinishContractException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDontFindFinishContractException e) {
        return ResponseService.getFailResult(ResultCode.DONT_FIND_FINISH_CONTRACT);
    }

    @ExceptionHandler(CNotRegistrationContractException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotRegistrationContractException e) {
        return ResponseService.getFailResult(ResultCode.NOT_REGISTRATION_CONTRACT);
    }

    @ExceptionHandler(CDontFindProgressContractException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDontFindProgressContractException e) {
        return ResponseService.getFailResult(ResultCode.DONT_FIND_PROGRESS_CONTRACT);
    }

    @ExceptionHandler(CDontFindNotUsingProcessInfoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDontFindNotUsingProcessInfoException e) {
        return ResponseService.getFailResult(ResultCode.DONT_FIND_NOT_USING_PROCESS_INFO);
    }

    @ExceptionHandler(CNotRegistrationProcessInfoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotRegistrationProcessInfoException e) {
        return ResponseService.getFailResult(ResultCode.NOT_REGISTRATION_PROCESS_INFO);
    }

    @ExceptionHandler(CDontFindProcessInfoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDontFindProcessInfoException e) {
        return ResponseService.getFailResult(ResultCode.DONT_FIND_PROCESS_INFO);
    }

    @ExceptionHandler(CDontFindWorkStatusException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDontFindWorkStatusException e) {
        return ResponseService.getFailResult(ResultCode.DONT_FIND_WORK_STATUS);
    }

    @ExceptionHandler(CNotRegistrationWorkStatusException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotRegistrationWorkStatusException e) {
        return ResponseService.getFailResult(ResultCode.NOT_REGISTRATION_WORK_STATUS);
    }

    @ExceptionHandler(CDontFindNotUsingPackageInfoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDontFindNotUsingPackageInfoException e) {
        return ResponseService.getFailResult(ResultCode.DONT_FIND_NOT_USING_PACKAGE_INFO);
    }

    @ExceptionHandler(CNotRegistrationPackageInfoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotRegistrationPackageInfoException e) {
        return ResponseService.getFailResult(ResultCode.NOT_REGISTRATION_PACKAGE_INFO);
    }

    @ExceptionHandler(CNotRegistrationWorkRecordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotRegistrationWorkRecordException e) {
        return ResponseService.getFailResult(ResultCode.NOT_REGISTRATION_WORK_RECORD);
    }

    @ExceptionHandler(CDontFindTeamInfoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDontFindTeamInfoException e) {
        return ResponseService.getFailResult(ResultCode.DONT_FIND_TEAM_INFO);
    }

    @ExceptionHandler(CDontFindMemberInfoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDontFindMemberInfoException e) {
        return ResponseService.getFailResult(ResultCode.DONT_FIND_MEMBER_INFO);
    }

}
