package shop.usehwa.gen.api.push.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.usehwa.gen.api.push.model.CustomerLoginRequest;
import shop.usehwa.gen.api.push.model.CustomerViewResponse;
import shop.usehwa.gen.api.push.service.CustomerViewService;
import shop.usehwa.gen.common.response.model.SingleResult;
import shop.usehwa.gen.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "고객용 진행도 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerViewController {
    private final CustomerViewService customerViewService;

    @ApiOperation(value = "고객용 진행도 확인")
    @PostMapping("/view")
    public SingleResult<CustomerViewResponse> getCustomerView(@RequestBody @Valid CustomerLoginRequest request) {
        return ResponseService.getSingleResult(customerViewService.getCustomerView(request));
    }
}
