package shop.usehwa.gen.api.push.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import shop.usehwa.gen.api.push.interceptors.HeaderRequestInterceptor;
import shop.usehwa.gen.common.exception.CMissingDataException;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Service
public class AndroidPushService {
    @Async
    public void callFCM(HttpEntity<String> entity) {
        final String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";
        final String FIREBASE_SERVER_KEY = "AAAAj4cPtf4:APA91bGogaQYtQkcsUayh6sm";

        RestTemplate restTemplate = new RestTemplate();

        ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new HeaderRequestInterceptor("Authorization",  "key=" + FIREBASE_SERVER_KEY));
        interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json; UTF-8 "));
        restTemplate.setInterceptors(interceptors);

        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
        restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);
    }

    public void sendPush(List<String> sendTokens, String titleText, String bodyText) {
        JSONObject body = new JSONObject();

        JSONArray tokens = new JSONArray();
        for (String item : sendTokens) {
            tokens.put(item);
        }
        body.put("registration_ids", tokens);

        JSONObject notification = new JSONObject();
        notification.put("title", titleText); // push 알림 타이틀
        notification.put("body", bodyText); // 내용
        body.put("notification", notification); // 넣어서 callFCM 통해서 보내기

        HttpEntity<String> request = new HttpEntity<>(body.toString());

        try {
            callFCM(request);
        } catch (Exception e) {
            throw new CMissingDataException(); // 푸시알림 전송실패 라고 보내기
        }
    }
}
