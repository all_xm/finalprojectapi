package shop.usehwa.gen.api.push.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SendPushRequest {
    @ApiModelProperty(notes = "푸시 제목")
    @NotNull
    @Length(min = 5, max = 40)
    private String pushTitle;

    @ApiModelProperty(notes = "푸시 내용")
    @NotNull
    @Length(min = 5, max = 80)
    private String pushBody;
}
