package shop.usehwa.gen.api.push.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.push.entity.Contract;
import shop.usehwa.gen.common.function.ConvertFormat;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CustomerViewResponse {
    @ApiModelProperty(notes = "고객명")
    private String customerName;

    @ApiModelProperty(notes = "계약 번호")
    private String contractNumber;

    @ApiModelProperty(notes = "계약일자")
    private String contractDate;

    @ApiModelProperty(notes = "패키지 이름")
    private String packageName;

    @ApiModelProperty(notes = "현재 공정")
    private String currentProgress;

    @ApiModelProperty(notes = "공정 현황 목록")
    private List<CustomerViewItem> processStatus;

    @ApiModelProperty(notes = "업무 종료 여부")
    private Boolean isEndWork;

    private CustomerViewResponse(CustomerViewResponseBuilder builder) {
        this.customerName = builder.customerName;
        this.contractNumber = builder.contractNumber;
        this.contractDate = builder.contractDate;
        this.packageName = builder.packageName;
        this.currentProgress = builder.currentProgress;
        this.processStatus = builder.processStatus;
        this.isEndWork = builder.isEndWork;
    }

    public static class CustomerViewResponseBuilder implements CommonModelBuilder<CustomerViewResponse> {

        private final String customerName;
        private final String contractNumber;
        private final String contractDate;
        private final String packageName;
        private final String currentProgress;
        private final List<CustomerViewItem> processStatus;
        private final Boolean isEndWork;

        public CustomerViewResponseBuilder(
                Contract contract,
                String currentProgress,
                List<CustomerViewItem> processStatus) {
            this.customerName = contract.getCustomerName();
            this.contractNumber = contract.getContractNumber();
            this.contractDate = ConvertFormat.convertLocalDateToString(contract.getContractDate());
            this.packageName = contract.getPackageInfo().getPackageName();
            this.currentProgress = currentProgress;
            this.processStatus = processStatus;

            if (contract.getEndWorkDate() != null) {
                this.isEndWork = true;
            } else {
                this.isEndWork = false;
            }
        }

        @Override
        public CustomerViewResponse build() {
            return new CustomerViewResponse(this);
        }
    }
}
