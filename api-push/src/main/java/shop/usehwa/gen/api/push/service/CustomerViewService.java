package shop.usehwa.gen.api.push.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.push.entity.Contract;
import shop.usehwa.gen.api.push.entity.Process;
import shop.usehwa.gen.api.push.entity.WorkStatus;
import shop.usehwa.gen.api.push.model.CustomerLoginRequest;
import shop.usehwa.gen.api.push.model.CustomerViewItem;
import shop.usehwa.gen.api.push.model.CustomerViewResponse;
import shop.usehwa.gen.api.push.repository.ContractRepository;
import shop.usehwa.gen.api.push.repository.ProcessRepository;
import shop.usehwa.gen.api.push.repository.WorkStatusRepository;
import shop.usehwa.gen.common.exception.CMissingDataException;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerViewService {
    private final WorkStatusRepository workStatusRepository;
    private final ProcessRepository processRepository;
    private final ContractRepository contractRepository;
    private final AppTokenService appTokenService;

    public CustomerViewResponse getCustomerView(CustomerLoginRequest request) {
        Contract contract = contractRepository.findByContractNumber(request.getContractNumber());
        if (!contract.getCustomerPhone().equals(request.getCustomerPhone())) throw new CMissingDataException();

        List<WorkStatus> workStatusList = workStatusRepository.findAllByContract_ContractNumberAndContract_CustomerPhone(request.getContractNumber(), request.getCustomerPhone());

        List<CustomerViewItem> result = new LinkedList<>();
        workStatusList.forEach(item -> result.add(new CustomerViewItem.CustomerViewItemBuilder(item).build()));

        List<Process> processList = processRepository.findByPackageInfoOrderByProcessOrderAsc(contract.getPackageInfo());

        int processCnt = processList.size();
        String currentProgress = contract.getPresentProcess().getProcessName() + (contract.getPresentProcess().getProcessOrder() / processCnt) * 100 + "%";

        appTokenService.settingToken(contract, request.getToken());

        return new CustomerViewResponse.CustomerViewResponseBuilder(contract, currentProgress, result).build();
    }
}
