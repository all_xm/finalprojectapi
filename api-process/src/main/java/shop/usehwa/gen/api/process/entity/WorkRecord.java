package shop.usehwa.gen.api.process.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WorkRecord {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "공정 관리 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "processId")
    private Process processId;

    @ApiModelProperty(notes = "팀 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "teamId")
    private Team teamId;

    @ApiModelProperty(notes = "사진 이미지 주소")
    @Column(length = 200)
    private String imageAddress;

    @ApiModelProperty(notes = "메모")
    @Column(columnDefinition = "TEXT")
    private String memo;

    @ApiModelProperty(notes = "작업자명")
    @Column(nullable = false, length = 20)
    private String workerName;

    @ApiModelProperty(notes = "계약서 관리 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "contractId")
    private Contract contract;

    @ApiModelProperty(notes = "등록일시")
    @Column(nullable = false)
    private LocalDateTime createDateTime;
}
