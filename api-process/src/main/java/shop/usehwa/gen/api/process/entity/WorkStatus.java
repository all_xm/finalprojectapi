package shop.usehwa.gen.api.process.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WorkStatus {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "공정 관리 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "processId")
    private Process process;

    @ApiModelProperty(notes = "팀 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "teamId")
    private Team team;

    @ApiModelProperty(notes = "계약서 관리 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "contractId")
    private Contract contract;

    @ApiModelProperty(notes = "완료 여부")
    @Column(nullable = false)
    private Boolean isComplete;

    @ApiModelProperty(notes = "업무 기록 ID")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mainWorkRecordId")
    private WorkRecord workRecord;

    @ApiModelProperty(notes = "승인자")
    @Column(length = 20)
    private String approver;

    @ApiModelProperty(notes = "등록일자")
    @Column(nullable = false)
    private LocalDateTime createDate;

}
