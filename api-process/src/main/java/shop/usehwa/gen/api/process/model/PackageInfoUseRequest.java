package shop.usehwa.gen.api.process.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PackageInfoUseRequest {

    @ApiModelProperty("사용여부")
    @NotNull
    private Boolean isUse;
}
