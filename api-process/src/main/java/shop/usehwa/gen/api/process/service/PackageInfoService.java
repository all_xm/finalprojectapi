package shop.usehwa.gen.api.process.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.process.entity.PackageInfo;
import shop.usehwa.gen.api.process.model.PackageInfoCreateRequest;
import shop.usehwa.gen.api.process.model.PackageInfoItem;
import shop.usehwa.gen.api.process.model.PackageInfoUseRequest;
import shop.usehwa.gen.api.process.repository.PackageInfoRepository;
import shop.usehwa.gen.common.exception.CDontFindNotUsingPackageInfoException;
import shop.usehwa.gen.common.exception.CNotRegistrationPackageInfoException;
import shop.usehwa.gen.common.response.model.ListResult;
import shop.usehwa.gen.common.response.service.ListConvertService;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PackageInfoService {

    private final PackageInfoRepository packageInfoRepository;

    public void setPackageInfo(PackageInfoCreateRequest request) {
        PackageInfo addData = new PackageInfo.PackageInfoBuilder(request).setMemo(request.getMemo()).build();
        packageInfoRepository.save(addData);
    }

    public ListResult<PackageInfoItem> getPackageInfos() {
        List<PackageInfo> originList = packageInfoRepository.findAll();

        if (originList.isEmpty()) throw new CNotRegistrationPackageInfoException();
        List<PackageInfoItem> result = new LinkedList<>();

        originList.forEach(item-> result.add(new PackageInfoItem.PackageInfoItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<PackageInfoItem> getUsePackageInfos() {
        List<PackageInfo> originList = packageInfoRepository.findByIsUse(true);

        if (originList.isEmpty()) throw new CDontFindNotUsingPackageInfoException();
        List<PackageInfoItem> result = new LinkedList<>();

        originList.forEach(item-> result.add(new PackageInfoItem.PackageInfoItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void putPackageInfo(long id, PackageInfoCreateRequest request) {
        PackageInfo originData = packageInfoRepository.findById(id).orElseThrow(CNotRegistrationPackageInfoException::new);
        originData.putPackageInfo(request);

        packageInfoRepository.save(originData);
    }

    public void putPackageInfoUse(long id, PackageInfoUseRequest request) {
        PackageInfo originData = packageInfoRepository.findById(id).orElseThrow(CNotRegistrationPackageInfoException::new);
        originData.putPackageInfoUse(request);

        packageInfoRepository.save(originData);
    }
}
