package shop.usehwa.gen.api.process.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.process.entity.PackageInfo;
import shop.usehwa.gen.api.process.entity.Process;
import shop.usehwa.gen.api.process.entity.Team;
import shop.usehwa.gen.api.process.model.ProcessCreateRequest;
import shop.usehwa.gen.api.process.model.ProcessInfoUpdateRequest;
import shop.usehwa.gen.api.process.model.ProcessItem;
import shop.usehwa.gen.api.process.model.ProcessUseUpdaterequest;
import shop.usehwa.gen.api.process.repository.PackageInfoRepository;
import shop.usehwa.gen.api.process.repository.ProcessRepository;
import shop.usehwa.gen.api.process.repository.TeamRepository;
import shop.usehwa.gen.common.exception.*;
import shop.usehwa.gen.common.response.model.ListResult;
import shop.usehwa.gen.common.response.service.ListConvertService;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProcessService {
    private final ProcessRepository processRepository;
    private final TeamRepository teamRepository;
    private final PackageInfoRepository packageInfoRepository;

    public void setProcess(ProcessCreateRequest request) {
        Team team = teamRepository.getById(request.getTeamId());
        PackageInfo packageInfo = packageInfoRepository.getById(request.getPackageInfoId());

        Process addData = new Process.Builder(team, packageInfo, request).setMemo(request.getMemo()).build();
        processRepository.save(addData);
    }

    public ListResult<ProcessItem> getAllProcess() {
        List<Process> originList = processRepository.findAll();

        if (originList.isEmpty()) throw new CNotRegistrationProcessInfoException();
        List<ProcessItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new ProcessItem.Builder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<ProcessItem> getUseProcess() {
        List<Process> originList = processRepository.findAllByIsUse(true);

        if (originList.isEmpty()) throw new CDontFindProcessInfoException();
        List<ProcessItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new ProcessItem.Builder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<ProcessItem> getUsePackageProcess(long packageInfoId) {
        PackageInfo packageInfo = packageInfoRepository.getById(packageInfoId);

        List<Process> originList = processRepository.findAllByIsUseAndPackageInfo(true, packageInfo);

        if (originList.isEmpty()) throw new CDontFindNotUsingProcessInfoException();
        List<ProcessItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new ProcessItem.Builder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<ProcessItem> searchProcessByPackageInfo(String packageInfoName) {
        List<Process> originList = processRepository.findByPackageInfo_PackageNameLike("%" + packageInfoName + "%");

        if (originList.isEmpty()) throw new CDontFindProcessInfoException();
        List<ProcessItem> result = new LinkedList<>();
        originList.forEach(item -> result.add(new ProcessItem.Builder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public ListResult<ProcessItem> searchProcessByProcessName(String processName) {
        List<Process> originList = processRepository.findByProcessNameLike("%" + processName + "%");

        if (originList.isEmpty()) throw new CDontFindProcessInfoException();
        List<ProcessItem> result = new LinkedList<>();
        originList.forEach(item -> result.add(new ProcessItem.Builder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void putProcessInfo(long processId, ProcessInfoUpdateRequest request) {
        Team team = teamRepository.getById(request.getTeamId());
        PackageInfo packageInfo = packageInfoRepository.getById(request.getPackageInfoId());

        Process addData = processRepository.getById(processId);
        addData.putProcessInfo(team, packageInfo);
        processRepository.save(addData);
    }

    public void putProcessUse(long processId, ProcessUseUpdaterequest request) {
        Process addData = processRepository.getById(processId);
        addData.putProcessUse(request);
        processRepository.save(addData);
    }
}
