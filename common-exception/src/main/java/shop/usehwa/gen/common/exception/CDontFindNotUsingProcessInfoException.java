package shop.usehwa.gen.common.exception;

public class CDontFindNotUsingProcessInfoException extends RuntimeException {
    public CDontFindNotUsingProcessInfoException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDontFindNotUsingProcessInfoException(String msg) {
        super(msg);
    }

    public CDontFindNotUsingProcessInfoException() {
        super();
    }
}