package shop.usehwa.gen.common.exception;

public class CDontFindContractException extends RuntimeException {
    public CDontFindContractException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDontFindContractException(String msg) {
        super(msg);
    }

    public CDontFindContractException() {
        super();
    }
}